\documentclass{article}

\usepackage[margin=1.5cm]{geometry}
\usepackage{amsmath}
\usepackage{bm}
\usepackage{mathtools}

\renewcommand{\d}{\mathrm{d}}
\newcommand{\mean}[1]{{\langle #1 \rangle}}

\begin{document}

\begin{flushright}
    {\large \textbf{Harry Austen}}
\end{flushright}

\begin{center}
    {\huge \textbf{Graphs \& Networks}}

    {\large \textbf{Summary Notes}}
\end{center}

\begin{itemize}
    \item A \textbf{network}/\textbf{graph} $G(V,E)$ is a collection of \textbf{vertices} $ v \in V = \{v_1,v_2,\hdots,v_N\} $, connected by \textbf{edges} $ e = (u,v) \in E = \{e_1,e_2,\hdots,e_M\} $
    \item The \textbf{adjacency matrix} $\bm{A}$ is an $N \times N$ symmetric matrix defined as
        \begin{equation*}
            A_{ij} =
            \begin{dcases}
                1 &\text{if } (i,j) \in E \\
                0 &\text{otherwise}
            \end{dcases}
        \end{equation*}
        for undirected networks and
        \begin{equation*}
            A_{ij} =
            \begin{dcases}
                1 &\text{if there is a node from $j$ to $i$} \\
                0 &\text{otherwise}
            \end{dcases}
        \end{equation*}
        for directed networks. For a multigraph the value $A_{ij}$ reflects the mult-edge multiplicity between the nodes
    \item The \textbf{number of edges} $M$ can be defined in terms of the adjacency matrix as
        $$ M = \frac{1}{2} \sum_{ij} A_{ij} $$
    \item The \textbf{degree} $k_i$ of a node $i$ is the number of edges it receives. It can be expressed as the sum of a row in the adjacency matrix
        \begin{equation*}
            k_i = \sum_j A_{ij}
        \end{equation*}
    \item The \textbf{mean degree} $\mean{k}$ is defined as
        \begin{align*}
            \frac{1}{N} \sum_i k_i &= \frac{1}{N} \sum_{ij} A_{ij} \\
            &= \frac{2M}{N}
        \end{align*}
    \item A network is \textbf{sparse} if $ \mean{k} \ll N $
    \item The \textbf{degree sequence} is the decreasing sequence of all degrees in the network, e.g.
        $$ \{k_1,k_2,\hdots,k_N\} \quad \text{where} \quad k_1 \ge k_2 \ge \hdots \ge k_N $$
    \item A \textbf{walk} is any sequence of nodes such that every consecutive pair of nodes in the sequence is connected by an edge
    \item A \textbf{path} is a walk with no repeated nodes
    \item An \textbf{edge path} is a walk with no repeated edges (but it can repeat nodes)
    \item A \textbf{multi-edge} is an edge with the same endpoints
    \item A \textbf{self-edge} is an edge that connects a node to itself
    \item A network without self-edges and multi-edges is called a \textbf{simple graph}
    \item A network with self-edges or multi-edges is called a \textbf{multigraph}
    \item For directed networks we differentiate between
        \begin{alignat*}{2}
            \text{the \textbf{in-degree} } &k_i^{\text{in}} &&= \sum_j A_{ij} \\
            \text{and \textbf{out-degree} } &k_i^{\text{out}} &&= \sum_i A_{ij}
        \end{alignat*}
    \item A \textbf{bipartite network} has two types of nodes (\textbf{parts}), and edges only connect nodes of different types
    \item A \textbf{tree} is a connected, undirected network with no loops
    \item A \textbf{leaf} is a node in a tree with degree one
    \item A \textbf{forest} is a collection of trees (hence disconnected)
    \item A network is \textbf{planar} if it can be drawn ona  plane without any crossing edges
    \item A network is \textbf{complete} when all possible edges exist (i.e. A complete network with $N$ nodes has $N-1$ edges, denoted $K_N$)
    \item A \textbf{clique} is a subset of a network which forms a complete graph
    \item The \textbf{shortest path} (\textbf{geodesic path}) $d_{ij}$ between two nodes $j$ and $i$ is the length of the shortest path between them (it is the smallest value of $r$ for which $(\bm{A})_{ij}>0$)
    \item The \textbf{diameter} $l$ of a network is the length of the longest shortest path
        $$ l = \max_{ij} d_{ij} $$
    \item A \textbf{component} is a maximal subset of nodes of a network, with at least one path between any two nodes in the subset
    \item A network with a single component is called \textbf{connected}. Similarly, a network with more than one component is called \textbf{disconnected}
    \item For directed networks, a \textbf{strong component} is a maximal subset of nodes, with at least one path between any two nodes, in \textbf{both directions} and a \textbf{weak component} only requires all nodes to be reached by eachother in a \textbf{single direction}
    \item A network with a single weak component is called \textbf{weakly connected} and similarly for \textbf{strongly connected}
    \item The \textbf{Laplacian} $\bm{L}$ for a simple, undirected network is an $N \times N$ symmetric matrix
        \begin{align*}
            L_{ij} &=
            \begin{cases}
                k_i &\text{if } i = j \\
                -1 &\text{if } A_{ij} = 1 \\
                0 &\text{otherwise}
            \end{cases} \\
            &= k_i\delta_{ij} - A_{ij}
        \end{align*}
        In matrix notation
        $$ \bm{L} = \bm{D} - \bm{A} $$
        where $D_{ij}=k_i\delta_{ij}$ is the diagonal matrix of degrees
    \item A \textbf{random walk} is a walk where each step is taken completely at random (i.e. starting at node $i$, move to a neighbour $j$ with probability $1/k_i$)
    \item The \textbf{degree centrality} of a node is equivalent to its degree (or in/out-degree in the case of directed networks)
    \item The \textbf{eigenvector centrality} is an extension of the degree centrality, where the centrality awarded to a node is proportional to the centrality of its (in-)neighbours
        \begin{align*}
            x_i &= \kappa^{-1} \sum_j A_{ij}x_j \\
            \implies \bm{Ax} &= \kappa\bm{x}
        \end{align*}
        where $\kappa$ is the leading eigenvalue of $\bm{A}$
    \item \textbf{Katz centrality} addresses some of the issues of eigenvector centrality by assigning a default centrality value $\beta$ independent of its (in-)neighbours
        \begin{align*}
            x_i &= \alpha \sum_j A_{ij}x_j + \beta \\
            \implies \bm{x} &= \alpha\bm{Ax} + \beta\bm{1}
        \end{align*}
        where $\bm{1}$ is the uniform vector $(1,1,\hdots)^T$ \par
        Convention is to set $\beta=1$ giving
        $$ \bm{x} = (\bm{I}-\alpha\bm{A})^{-1}\bm{1} $$
    \item \textbf{PageRank} modifies Katz centrality by dividing the inherited centrality by the in-neighbour's out-degree
        \begin{align*}
            x_i &= \alpha \sum_j A_{ij} \frac{x_j}{k_j^{\text{out}}} + \beta \\
            \implies \bm{x} &= \alpha \bm{AD}^{-1}\bm{x} + \beta\bm{1}
        \end{align*}
        This \textbf{dilutes} the \textbf{prestige} of a node among all of its out-neighbours. Again $\beta=1$ by default giving
        $$ \bm{x} = \left( \bm{I}-\alpha\bm{AD}^{-1} \right)^{-1} \bm{1} $$
        with $\alpha=0.85$ as a default used by Google
    \item \textbf{Closeness centrality} is based on the distance between nodes
        $$ C_i = \frac{1}{l_i} = \frac{N}{\sum_j d_{ij}} $$
    \item \textbf{Betweenness centrality} rates the importance of a node by how often it is visited part-way along the shortest paths between every other node
        $$ x_i = \sum_{ij} \frac{n^k_{ij}}{n_{ij}} $$
        where $n_{ij}$ is the number of shortest paths from $i$ to $j$ and $n^k_{ij}$ is the number of shortest paths from $i$ to $j$ that pass through $k$
    \item \textbf{Transitivity} is the tendency of two neighbours of the same node to also be connected by an edge
    \item Transitivity can be measured by the \textbf{clustering coefficient}
        $$ C = \frac{(\text{number of triangles}) \times 3}{(\text{number of connected triples})} $$
        where a \textbf{connected triple} is three nodes $(i,j,k)$ with edges $(i,j)$ and $(j,k)$ (but not necessarily $(k,i)$)
    \item For directed networks, \textbf{reciprocity} measures the frequency of loops of length two, i.e. connections from $i$ to $j$ that are reciprocated from $j$ to $i$
        \begin{align*}
            r &= \frac{1}{M} \sum_{ij} A_{ij}A_{ji} \\
            &= \frac{1}{M} \text{tr}\bm{A}^2
        \end{align*}
    \item \textbf{Homophily}/\textbf{assortative mixing} is the tendency of nodes of the same type to connect more often
    \item The mean difference between observed connections and the expected connections of a random network between nodes of the same type is given by the \textbf{modularity}
        \begin{align*}
            Q &= \frac{1}{2M} \sum_{ij} \left( A_{ij} - \frac{k_ik_j}{2M} \right) \delta_{g_i,g_j} \\
            &\in [-1,1]
        \end{align*}
        A value of $Q>0$ implies \textbf{assortative mixing} and $Q<0$ implies \textbf{dissortative mixing}
    \item \textbf{Breadth-first search} (BFS) can be used to determine the shortest distances from a node to every other node in the network in $O(N+M)$ runtime.It can also be altered to keep track of \textbf{predecessors} and hence reverse the tracked predecessors to obtain the shortest paths to each node
    \item The \textbf{largest component} is quantified by the fraction $S$ of nodes that belong to it
    \item The \textbf{in-component} of a node $i$ is the set of nodes with a path leading to $i$ (including $i$ itself)
    \item The \textbf{out-component} of a node $i$ is the set of nodes that can be reached from $i$ with a path
    \item The \textbf{mean distance} $l_i$ of node $i$ to every other node is given by
        $$ l_i = \frac{1}{N.. \sum_j d_{ij}} $$
        which can be used to determine the \textbf{mean distance} $l$ between all nodes in the network
        \begin{align*}
            l &= \frac{1}{N} \sum_i l_i \\
            &= \frac{1}{N^2} \sum_{ij} d_{ij}
        \end{align*}
    \item A network possesses the \textbf{small-world property} if its mean distance scales as
        $$ l \propto \log N $$
        or slower with the number of nodes $N$
    \item The \textbf{degree distribution} $p_k$ of a network is the fraction of nodes that have degree $k$
    \item A network is \textbf{scale free} if its degree distribution takes the form of the \textbf{power law distribution}
        $$ p_k = Ck^{-\alpha} $$
    \item The $\bm{G(N,M)}$ \textbf{model} is the random construction of a graph, where from a set of $N$ nodes, one selects $M$ edges uniformly at random from the set of all possible edges, and places them on the graph
    \item The $\bm{G(N,p)}$ \textbf{model} is the random construction of a graph, where for each node pair $i,j$, the edge $(i,j)$ is sampled independently with the same probability $p \in [0,1]$. The degree distribution of the $G(N,p)$ model is known as the \textbf{binomial distribution}
        $$ p_k = \binom{N-1}{k} p^k(1-p)^{N-1-k} $$
        where the \textbf{binomial coefficient} is defined as
        $$ \binom{x}{y} = \frac{x!}{y!(x-y)!} $$
    \item The \textbf{configuration model} takes as parameters an imposed degree sequence $ \bm{k} = \{k_1,\hdots,k_N\} $. The edge end-points (stubs/half-edges) are placed on each node according to $\bm{k}$ and connected at random to form a multigraph
    \item The \textbf{excess degree} is the number of edges attached to a neighbour (minus the one we used to reach it)
    \item The \textbf{excess degree distribution} $q_k$ is the probability that, by following a randomly chosen neighbour of a node, we will reach a node with excess degree $k$
    \item The \textbf{generating function} of a discrete probability distribution $p_k$ is the polynomial
        \begin{align*}
            g(z) &= p_0 + p_1z + p_2z^2 + \hdots \\
            &= \sum_k p_kz^k
        \end{align*}
        which \textbf{generates} $p_k$ by differentiating it
        $$ p_k = \left. \frac{1}{k!} \frac{\d^k g(z)}{\d z^k} \right|_{z=0} $$
    \item Let $g_0(z) = \sum p_kz^k$ and $g_1(z) = \sum q_kz^k$ be the generating functions for the degree and excess degree distributions respectively, then
        \begin{align*}
            g_1(z) &= \frac{g_0'(z)}{g_0'(1)} \\
            \text{and} \quad S &= 1 - g_0(u)
        \end{align*}
        where $u$ is the probability that a node does not belong to the giant component
    \item \textbf{Preferential attachment} describes a process whereby the network grows in time, via the addition of new nodes and edges, connecting to old nodes with higher probability for nodes with higher degree (\textbf{rich get richer})
    \item \textbf{Price's model} is the simplest model, describing a \textbf{growing directed network}, where edges are attached to nodes with \textbf{probability proportional to its in-degree plus a constant $\bm{\alpha>0}$}
    \item \textbf{Gamma function}
        \begin{align*}
            \Gamma(x) &= \int_0^\infty t^{x-1}e^{-t} \d t \\
            &= (x-1) \cdot \Gamma(x-1) \\
            &= (x-1)!
        \end{align*}
    \item \textbf{Euler's beta function}
        $$ \mathcal{B}(x,y) = \frac{\Gamma(x)\Gamma(y)}{\Gamma(x+y)} $$
    \item \textbf{Barab\r{a}si and Albert's model} defines a \textbf{growing undirected network} by introducing new nodes with degree exactly equal to $c$, where edges connect to old nodes with probability proportional to their degree
    \item \textbf{Percolation} is the progressive removal of nodes (\textbf{site percolation}) or edges (\textbf{edge percolation}) of a network and determining if it remains connected afterwards. Nodes/edges are removed with probability $1-\phi$ where $\phi$ is called is called the \textbf{occupation probability}
    \item The basic \textbf{SI model} for \textbf{epidemics} is a mixed population of $N$ individuals, made up of $X$ infected and S susceptible, with people meeting each other at random with a \textbf{transmission/infection rate} $\beta$, giving the time derivatives
        $$ \frac{\d X}{\d t} = \beta\frac{SX}{N} \quad \text{and} \quad \frac{\d S}{\d t} = -\beta\frac{SX}{N} $$
        Letting $s = \frac{S}{N}$ and $x=\frac{X}{N}$ be the fractions of susceptible and infected individuals respectively
        \begin{align*}
            \frac{\d x}{\d t} = \beta sx \quad \text{and} \quad \frac{\d s}{\d t} &= -\beta sx \\
            \text{where} \quad x+s&=1 \\
            \implies \frac{\d x}{\d t} &= \beta(1-x)x
        \end{align*}
        which is called the \textbf{logistic equation} and solving for $x$ gives the \textbf{logistic growth curve}
        \begin{center}
            \includegraphics[width=.4\textwidth]{img/growth.png}
        \end{center}
    \item The \textbf{SIR model} is slightly more realistic allowing for individuals to \textbf{recover} from disease, acquiring immunity at a rate $\gamma$, taking $r = \frac{R}{N}$ as the fraction of recovered individuals
        $$ \frac{\d r}{\d t} = \gamma x $$
    \item The \textbf{SIS model} on the other hand, does not allow immunity, but instead individuals can become \textbf{reinfected}
        $$ \frac{\d s}{\d t} = \gamma x - \beta sx \quad \text{and} \quad \frac{\d x}{\d t} = \beta sx - \gamma x $$
    \item A \textbf{community} is a group of nodes with more internal than external edges
    \item \textbf{Community detection} is the task of identifying communities in a network, only by inspecting its structure. Some methods for doing this include \textbf{modularity maximisation} and \textbf{statistical inference of the stochastic block model}
    \item \textbf{Modularity maximisation} consists of finding a partition $\bm{g}=\{g_i\}$ of communities that maximises the modularity
        $$ Q = \frac{1}{2M} \sum_{ij} \left( A_{ij} - \frac{k_ik_j}{2M} \right) \delta_{g_i,g_j} $$
\end{itemize}

\end{document}
