\documentclass{article}

\usepackage[margin=15mm]{geometry}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{bm}
\usepackage{mathtools}

\renewcommand{\d}{\mathrm{d}}
\renewcommand{\k}{{\langle k \rangle}}

\newcommand{\mean}[1]{{\langle #1 \rangle}}

\begin{document}

\begin{flushright}
    {\large \textbf{Harry Austen}}
\end{flushright}

\begin{center}
    {\huge \textbf{Graphs \& Networks}}

    {\large \textbf{Mock Paper}}
\end{center}

\begin{enumerate}
    % Question 1
    \item
        \begin{enumerate}
            % Part A
            \item Define the terms \textbf{degree}, \textbf{degree sequence}, \textbf{adjacency matrix}, \textbf{walk} and \textbf{path}
                \begin{itemize}
                    \item The \textbf{degree}, $k_i$ of a node $i$ is the number of edges it recieves
                    \item The \textbf{degree sequence} is the decreasing sequence of all degrees in the network, i.e. $ \{ k_1, k_2, \hdots, k_N \} $
                    \item The \textbf{adjacency matrix}, $\bm{A}$, is an $N \times N$ symmetric matrix defined as
                        $$ A_{ij} =
                        \begin{cases}
                            1 &\text{if } (i,j) \in E \\
                            0 &\text{otherwise}
                        \end{cases}
                        $$
                    \item A \textbf{walk} is any sequence of nodes, such that every consecutive pair of nodes in the sequence is connected by an edge
                    \item A \textbf{path} is a walk with no repeated nodes
                \end{itemize}
            % Part B
            \item Write an expression for the degree $k_i$ of vertex $i$ and the total number of edges $M$, using only the adjacency matrix
                \begin{itemize}
                    \item \textbf{degree}
                        $$ k_i = \sum_j A_{ij} $$
                    \item \textbf{total number of edges}
                        $$ M = \frac{1}{2} \sum_{ij} A_{ij} $$
                \end{itemize}
            % Part C
            \item Prove that trees are bipartite
                \begin{itemize}
                    \item \textbf{Idea} \par
                        The even depth nodes of a tree only connect to the odd depth nodes and vice versa
                    \item \textbf{Proof} \par
                        Let $ G = (V,E) $ be a tree and pick any vertex $u \in V$ to be the \textbf{root}. Let $P_{uv}$ denote the unique path in $G$ from $u$ to $v$ for any other vertex $v \in V$. Assign the vertices to two parts such that
                        \begin{equation*}
                            v \in
                            \begin{cases}
                                X &, \text{if $|P_{uv}|$ is even} \\
                                Y &, \text{if $|P_{uv}|$ is odd}
                            \end{cases}
                        \end{equation*}
                        Since the paths are unique, we have that $|P_{uv}|$ is either odd or even, hence
                        \begin{align*}
                            X \cup Y &= V \\
                            \text{and} \quad X \cap Y &= \emptyset
                        \end{align*}
                        Suppose $ e = ab \in E $ is an edge. Note that $P_{ua}^{-1}P_{ub}$ is a walk from $a$ to $b$, which contains the unique path $e$. Without loss of generality, assume $ e \in P_{ua} $, therefore $P_{ub}e$ is a path of length $ |P_{ub}| + 1 $ from the root $u$ to $a$. Hence $|P_{ua}|$ and $|P_{ub}|$ have opposite parity and $a$ and $b$ are in different parts
                \end{itemize}
            % Part D
            \item Define the clustering coefficient and express it in terms of the adjacency matrix
                \begin{itemize}
                    \item The \textbf{clustering coefficient} is a value used to measure \textbf{transitivity}, which is the tendency of two neighbours of the same node also being connected by an edge. It is defined as
                        $$ C = \frac{(\text{number of triangles})\times 3}{(\text{number of connected triples})} $$
                        where a \textbf{connected triple} is three nodes connected by at least two edges
                    \item Since a triangle is a walk of length 3 that returns to the origin, the number of triangles in the network is
                        $$ \frac{1}{6} \text{tr} \bm{A}^3 $$
                        since each triangle corresponds to 6 closed walks. Likewise, connected triples are walks of length two, hence their number is
                        $$ \frac{1}{2} \sum_{i \neq j} (\bm{A}^2)_{ij} $$
                        Combining these, we have
                        $$ C = \frac{\text{tr}(\bm{A}^3)}{\sum_{i \neq j} \left( \bm{A}^2 \right)_{ij} } $$
                \end{itemize}
            % Part E
            \item You are asked to calculate the closeness centrality of a single node in an undirected network with $M$ edges and $N$ nodes. What algorithm would you use to do this and what would be the computational complexity of the algorithm?
                \begin{itemize}
                    \item \textbf{Closeness centrality} is defined as
                        $$ c_i = \frac{1}{l_i} = \frac{N}{\sum_j d_{ij}} $$
                        where $d_{ij}$ is the shortest distance from node $i$ to node $j$
                    \item We can perform breadth-first search starting from node $i$ to find the distance to all the others, then average those and take the reciprocal to get the closeness centrality
                    \item BFS takes $O(M+N)$ and averaging takes $O(N)$, therefore the overall runtime is
                        $$ O(M+N) $$
                \end{itemize}
            % Part F
            \item An \textbf{Euler path} is a walk on a graph that visits every edge exactly once. Prove that such a walk exists if and only if there are exactly zero or two vertices with odd degree, and the vertices with nonzero degree belong to a single component
                \begin{itemize}
                    \item If all edges are visited in a graph, all vertices of nonzero degree must be connected. For every vertex visited, with the exception of the first and last ones, the walk must \textbf{enter} and \textbf{leave} the vertex an equal number of times, via different edges and hence their degrees must be even
                    \item If the first and last vertices visited are different, they must have odd degree, corresponding to the first and last edges of the walk
                    \item If a graph has a single component with nonzero edges and only two vertices with odd degree, we can choose either of these as the start point. We can then choose any incident edge by visiting it. This holds until we land the final time on the remaining vertex of odd degree and finish the walk
                    \item If all nodes have even degree, any of them can be used as the starting point and we repeat the above, ending on that same starting node
                \end{itemize}
        \end{enumerate}
    % Question 2
    \item
        \begin{enumerate}
            % Part A
            \item Define a \textbf{locally tree-like graph} and the \textbf{small world property}
                \begin{itemize}
                    \item For a \textbf{locally tree-like graph}, if you start at any node in the network and obtain the set of all nodes at a distance $d$ or less from that starting node, the set will, with probability tending to 1 as $N \rightarrow \infty$, take the form of a tree
                    \item The \textbf{small world property} occurs when the \textbf{mean distance} $\ell$ between two randomly chosen nodes, grows proportionally (or slower) to the logarithm of the number of nodes in the network, i.e.
                        $$ \ell \propto \log N $$
                \end{itemize}
            % Part B
            \item Define the \textbf{Barab\'asi-Albert model} of graph growth by preferential attachment and state its mean degree and mean excess degree
                \begin{itemize}
                    \item The Barab\'asi Albert model defines a \textbf{growing undirected network} as follows
                        \begin{enumerate}
                            \item Start with a small arbitrary network with at least one node
                            \item At each time step, add a new node with degree $c$
                            \item Each edge of the new node connects with an old node, with a probability proportional to its degree
                            \item After all edges have been placed, proceed to the next time step and add a new node
                        \end{enumerate}
                    \item Each new node brings in $c$ edges, with $2c$ endpoints, thus the mean degree is $2c$
                    \item The degree distribution is $ p_k \approx k^{-3} $ and hence the excess degree distribution is $ q_k \approx k^{-2} $, which has a diverging first moment. Thus the mean excess degree grows to infinity
                \end{itemize}
            % Part C
            \item Prove that the average clustering coefficient vanishes for the configuration model as $N \rightarrow \infty$, where $N$ is the number of vertices
                \begin{itemize}
                    \item Suppose a node $v$ has two neighbours, $i$ and $j$. Let $k_i$ and $k_j$ be the excess degrees of the two neighbours (i.e. the number of edges incident on them, other than the one connecting either of them to $v$). The probability of an edge between $i$ and $j$ is then $ k_ik_j/2M $. The clustering coefficient is then obtained by averaging over $k_i$ and $k_j$:
                        \begin{align*}
                            C &= \sum_{k_ik_j} q_{k_i}q_{k_j}\frac{k_ik_j}{2M} \\
                            &= \frac{1}{2M} \left[ \sum_k kq_k \right]^2 \\
                            &\ \vdots \\
                            &= \frac{1}{N} \frac{[\langle k^2 \rangle - \k ]^2}{\k^3}
                        \end{align*}
                        where we used $ N \k = 2M $. From the above, we can see that $ C \propto 1/N $, and hence vanishes for large $N$
                \end{itemize}
            % Part D
            \item Consider a configuration model with degrees given by a geometric distribution
                $$ p_k = p(1-p)^k $$
                Using the generating function of the excess degree distribution, calculate the percolation threshold of this model, i.e. the value of the node occupation probability $\phi$ for which the giant cluster begins to appear
                \begin{itemize}
                    \item The generating function for the degree and excess degree distributions are
                        \begin{align*}
                            g_0(z) &= \sum_k p_kz^k = \frac{p}{1-(1-p)z} \\
                            \text{and} \quad g_1(z) &= \frac{g_0'(z)}{g_0'(1)} = \left( \frac{p}{1-(1-p)z} \right)^2 \\
                            \implies g_1'(z) &= \frac{2(1-p)p^2}{(1-(1-p)z)^3}
                        \end{align*}
                        Setting $z=1$ we get
                        $$ \phi_c = \frac{1}{g_1'(1)} = \frac{p}{2(1-p)} $$
                \end{itemize}
        \end{enumerate}
    % Question 3
    \item
        \begin{enumerate}
            % Part A
            \item Define the terms \textbf{degree distribution} and \textbf{excess degree distribution}
                \begin{itemize}
                    \item The \textbf{degree distribution} $p_k$ is the fraction of nodes that have degree equal to $k$
                    \item The \textbf{excess degree distribution} $q_k$ is the probability that, by following a randomly chosen neighbour of a node, we will reach a node with excess degree $k$
                \end{itemize}
            % Part B
            \item Define the following kinds of networks: \textbf{tree}, \textbf{bipartite}, \textbf{planar}, \textbf{complete}
                \begin{itemize}
                    \item A \textbf{tree} is a connected, undirected network that contains no loops
                    \item A \textbf{bipartite} network consists of only two types of nodes, and edges only exist between nodes of different types
                    \item A network is \textbf{planar} if it can be drawn on a plane without any edges crossing
                    \item A network is \textbf{complete} if a unique edge exists between every pair of nodes (i.e. every node has $N-1$ neighbours)
                \end{itemize}
            % Part C
            \item Prove that the clustering coefficient of bipartite networks and trees is zero
                \begin{itemize}
                    \item A triangle is not a bipartite graph, since if one node is in one partition, the two neighbours need to be in the other, but they share an edge. For the same reason, a triangle cannot be a subgraph of a bipartite network, and hence bipartite graphs cannot have trangles and therefore the clustering coefficient must be zero
                    \item A triangle is a loop of length three and trees do not have loops, thus the clustering coefficient of a tree is zero
                \end{itemize}
            % Part D
            \item Calculating an expression for the clustering coefficient of the configuration model, and show that it vanishes in the limit $N \rightarrow \infty$ where $N$ is the number of nodes
                \begin{itemize}
                    \item In question 2c, we prove that the clustering coefficient of the configuration model is given by
                        $$ C = \frac{1}{N} \frac{[ \langle k^2 \rangle - \k ]^2}{\k^3} $$
                        Therefore vanishes for large $N$
                \end{itemize}
            % Part E
            \item Consider the following model of a graph growing without preferential attachment: Starting with the complete graph on $M$ vertices, new vertices are added one at a time, each connecting to $M$ other vertices chosen uniformly at random from the existing graph. In this model, it can be shown that the limiting degree distribution obeys the difference equation
                $$ (M+1)p_k = Mp_{k-1} + \delta_{k,M} $$
                \begin{enumerate}
                    % Part I
                    \item Solve this equation for $p_k$ using the method of generating functions
                        \begin{itemize}
                            \item The generating function is defined by
                                $$ g_0(z) = \sum_k p_kz^k $$
                                Hence, if we multiply both sides of the difference equation by $z^k$ and sum over $k$, we obtain
                                \begin{align*}
                                    (M+1)g_0(z) &= \sum_k z^k (Mp_{k-1} + \delta_{k,M}) \\
                                    &= Mzg_0(z) + z^M \\
                                    \implies g_0(z) &= \frac{z^M}{M+1-Mz} \\
                                    &= \frac{z^M}{M+1} \cdot \frac{1}{1-\frac{M}{M+1}z} \\
                                    &= \frac{z^M}{M+1} \sum_k \left( \frac{M}{M+1} \right)^k z^k \\
                                    &= \sum_{k=0}^\infty \frac{M^k}{(M+1)^{k+1}} z^{k+M} \\
                                    &= \sum_{k=M}^\infty \frac{M^{k-M}}{(M+1)^{k-M+1}} z^{k}
                                \end{align*}
                                where we used the equation for a geometric series
                                \begin{equation*}
                                    \sum_k r^k = \frac{1}{1-r}
                                \end{equation*}
                                Hence reading off the coefficient of $z^k$, we get
                                \begin{equation*}
                                    p_k =
                                    \begin{dcases}
                                        0 &\text{for } k < M \\
                                        \frac{M^{k-M}}{(M+1)^{k-M+1}} &\text{for } k \ge M
                                    \end{dcases}
                                \end{equation*}
                        \end{itemize}
                    % Part II
                    \item Compute the limiting mean degree and mean excess degree in this non-preferential attachment model
                        \begin{itemize}
                            \item The mean degree can be expressed in terms of the generating function
                                $$ g_0'(1) = \sum_k kp_k = \k $$
                                therefore we can calculate the mean degree using our expression for $g_0(z)$ from the previous question
                                \begin{align*}
                                    g_0(z) &= \frac{z^M}{M+1-Mz} \\
                                    \implies g_0'(z) &= \frac{Mz^{M-1}}{M+1-Mz} + \frac{Mz^M}{(M+1-Mz)^2} \\
                                    \implies \k &= g_0'(1) = 2M
                                \end{align*}
                            \item We can express the mean excess degree in terms of the generating function
                                \begin{align*}
                                    g_1'(1) &= \sum_k kq_k \\
                                    \text{and} \quad g_1(z) &= \frac{g_0'(z)}{g_0'(1)}
                                \end{align*}
                                Hence we first need to calculate $g_0''(1)$ by differentiating our previous expression for $g_0'(z)$ and evaluating it at $z=1$
                                \begin{align*}
                                    g_0''(z) &= \frac{\d}{\d z} \left[ \frac{Mz^{M-1}}{M+1-Mz} + \frac{Mz^M}{(M+1-Mz)^2} \right] \\
                                    &= \frac{M(M-1)z^{M-2}}{M+1-Mz} + \frac{M^2z^{M-1}}{(M+1-Mz)^2} + \frac{M^2z^{M-1}}{(M+1-Mz)^2} + \frac{2M^2z^M}{(M+1-Mz)^3} \\
                                    \implies g_0''(1) &= 5M^2-M
                                \end{align*}
                                We can then divide this by $g_0'(z)$, which we calculated previously, to obtain the mean excess degree
                                \begin{align*}
                                    \sum_k kq_k &= \frac{g_0''(1)}{g_0'(z)} \\
                                    &= \frac{5M^2-M}{2M} \\
                                    &= \frac{5M-1}{2}
                                \end{align*}
                        \end{itemize}
                \end{enumerate}
        \end{enumerate}
    % Question 4
    \item
        \begin{enumerate}
            % Part A
            \item Prove that the mean degree of a tree is strictly less than 2
                \begin{itemize}
                    \item A tree is a connect graph of $N$ nodes and $M=N-1$ edges, therefore the mean degree is
                        $$ \k = \frac{2M}{N} = \frac{2(N-1)}{N} = 2 - \frac{2}{N} < 2 $$
                \end{itemize}
            % Part B
            \item Consider a network model in which the edges are placed independently between each pair of nodes $i,j$ with probability $ p_{ij} = Kf_if_j $, where $K$ is a constant and $f_i$ is a number associated to node $i$. Find an expression for the expected degree of node $i$
                \begin{itemize}
                    \item The expected degree of node $i$ is
                        $$ \mean{k_i} = \sum_j p_{ij} = Kf_i \sum_j f_j $$
                \end{itemize}
            % Part C
            \item A \textbf{star graph} consists of a single central node with $N-1$ other nodes connected to it thus:
                \begin{center}
                    \includegraphics[width=.2\textwidth]{img/star.png}
                \end{center}
                What is the largest (most positive) eigenvalue of the adjacency matrix of this network? \par
                (Hint: You can use the fact that for the adjacency matrix of a connected graph, there is only one eigenvector that has all its elements non-negative, and that is the eigenvector corresponding to the largest eigenvalue)
                \begin{itemize}
                    \item Let the eigenvector element at the central node be $x_1$. By symmetry, the elements at the peripheral nodes all have the same value. Let us denote this value $x_2$. The the eignevalue equation looks like this
                        \begin{equation*}
                            \begin{bmatrix}
                                0 & 1 & 1 & \hdots \\
                                1 & 0 & 0 & \hdots \\
                                1 & 0 & 0 & \hdots \\
                                \vdots & \vdots & \vdots & \ddots
                            \end{bmatrix}
                            \begin{bmatrix}
                                x_1 \\
                                x_2 \\
                                x_2 \\
                                \vdots
                            \end{bmatrix}
                            = \lambda
                            \begin{bmatrix}
                                x_1 \\
                                x_2 \\
                                x_2 \\
                                \vdots
                            \end{bmatrix}
                        \end{equation*}
                        where $\lambda$ is the leading eigenvalue. This implies that
                        $$ (N-1)x_2 = \lambda x_1 \quad \text{and} \quad x_1 = \lambda x_2 $$
                        Eliminating $x_1$ and $x_2$ from these equations, we find that
                        $$ \lambda = \sqrt{N-1} $$
                        The equation $ x_1 = \lambda x_2 $ then implies that $x_1$ and $x_2$ have the same sign, which means that this is the leading eignevalue (using the hint)
                \end{itemize}
            % Part D
            \item Find all strongly connected components in this graph:
                \begin{center}
                    \includegraphics[width=.2\textwidth]{img/strong-graph.png}
                \end{center}
                \begin{center}
                    \underline{ANSWER} \par
                    \includegraphics[width=.2\textwidth]{img/strong-graph-answer.png}
                \end{center}
            % Part E
            \item Consider the \textbf{stub-matching} process of generating networks with prescribed degree sequences, where to each node is ascribed $k_i$ stubs (or \textbf{half-edges}), and then the stubs are matched randomly to form a multigraph. Due to the possible presence of multiedges, more than one such matching can result in the same network. For a given sampled network with adjacency matrix \textbf{A}, show that the actual number of matchings corresponding to it is given by
                $$ \frac{\prod_i k_i !}{\prod_{i<j} A_{ij} ! \prod_i A_{ii} !!} $$
                where $n!!$ is called the double factorial of $n$, which is equal to $n(n-2)(n-4)...2$ if $n$ is even and $ 0!! = 1 $
                \begin{itemize}
                    \item Since the number of permutations of the $k_i$ stubs at node $i$ is $k_i!$, this seems to imply that the number of matchings corresponding to each network is $ \prod_i k_i! $. However, if the graphs contain self-edges or multiedges, then not all permutations of the stubs in the network result in a new matching
                    \item Consider, for instance, two nodes $i,j$ with a double edge between them, such that stub $X$ on one node is connected to stub $X$ on the other and stub $Y$ is connected to stub $Y$. Now if we permute the two stubs on one of the nodes we will have $X$ connected to $Y$ and $Y$ connected to $X$, but if we permute the stubs on both nodes simultaneously, we still have $X$ to $X$ and $Y$ to $Y$ (the matching has not changed)
                    \item Indeed, we can see that for a multiedge with multiplicity $A_{ij}$, any permutation of the stubs at one end has no effect on the matching if we perform the same permutation at the other end
                    \item There are $A_{ij}!$ permutations of the multiedge, which gives the factor of $ \prod_{i<j} A_{ij}! $ in the question
                    \item For self-edges, the argument is similar. If we have $\frac{1}{2} A_{ii} $ self-edges at node $i$, then any permutation of stubs at one end of each edge has no effect on the matching if we permute those at the other ends in the same way, for a factor of $(\frac{1}{2}A_{ii})!$
                    \item But, in addition, we can also swap the two stubs at opposite ends of the same self-edge and it will have no effect on the matching, which gives us a factor of $ 2^{A_{ii}/2} $. So overall, we have
                        \begin{align*}
                            2^{A_{ii}/2}(\frac{1}{2}A_{ii})! &= 2^{A_{ii}/2}(1 \times 2 \times \hdots \times \frac{1}{2}A_{ii}) \\
                            &= 2 \times 4 \times \hdots \times A_{ii} \\
                            &= A_{ii}!!
                        \end{align*}
                    \item Putting everything together, we get the expression in the question
                \end{itemize}
        \end{enumerate}
\end{enumerate}

\end{document}
